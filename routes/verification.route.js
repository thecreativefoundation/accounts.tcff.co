const express = require('express');
const router = express.Router();

const Account = require("../models/account.model.js");
const VerificationCode = require("../models/verificationCode.model");
const TokenUtil = require("../utils/token.util");

const NotificationUtil = require("../utils/notification.util");
const UserActivityUtil = require("../utils/userActivity.util");

router.post("/verify", (req, res) => {
    const givenCode = req.body.code;
    const username = req.body.username;

    NotificationUtil.sendSlackMessage("8: verifying code:"+givenCode+" for username:"+username, "CF3U0LPDF");
    UserActivityUtil.logActivity("codeVerification", req.body.ipData, username);

    VerificationCode.findOne({'username': username}).sort({'created_at': -1}).exec((error, data) => {
        if (error) {
            res.status(202).json({
                statusCode: 505,
                timestamp: Date.now(),
                message: "operation failed",
                data: error
            });
        }
        else {
            if (data.code === givenCode) {
                if (req.query.route === "signup") {
                    Account.findOne({'username': username}).exec((error, user) => {
                        Account.findByIdAndUpdate(user.id, {isVerified: true}).exec((error, newUser) => {
                            if (error) {
                                NotificationUtil.sendSlackMessage("505: error during /verification/verify", "CF3U0LPDF");
                                res.status(202).json({
                                    status: 505,
                                    timestamp: Date.now(),
                                    message: "updating verification status failed, but user is indeed verified",
                                    data: error
                                });
                            } else {
                                VerificationCode.findByIdAndRemove(data.id).exec((error, info) => {
                                    let err = false;
                                    if (error) {
                                        NotificationUtil.sendSlackMessage("505: error during /verification/verify", "CF3U0LPDF");
                                        err = error;
                                    }
                                    TokenUtil.generateSecureJWT(newUser.username, newUser.email, newUser.meta.firstName, newUser.meta.lastName).then((token) => {
                                        res.status(202).json({
                                            statusCode: 202,
                                            timestamp: Date.now(),
                                            message: "verification and generating auth token passed",
                                            data: {
                                                token: token,
                                                username: newUser.username,
                                                info: info,
                                                errorDuringRemoval: err
                                            }
                                        });
                                    }).catch((error) => {
                                        NotificationUtil.sendSlackMessage("505: error during /verification/verify", "CF3U0LPDF");
                                        res.status(202).json({
                                            statusCode: 505,
                                            timestamp: Date.now(),
                                            message: "generating an auth token failed",
                                            data: error
                                        });
                                    });
                                });
                            }
                        });
                    });
                }
                else if (req.query.route === "signin") {
                    VerificationCode.findByIdAndRemove(data.id).exec((error, info) => {
                        let err = false;
                        if (error) {
                            NotificationUtil.sendSlackMessage("505: error during /verification/verify", "CF3U0LPDF");
                            err = error;
                        }
                        TokenUtil.generateSecureJWT().then((token) => {
                            res.status(202).json({
                                statusCode: 202,
                                timestamp: Date.now(),
                                message: "verification and generating auth token passed",
                                data: {
                                    token: token,
                                    username: req.body.username,
                                    info: info
                                }
                            });
                        }).catch((error) => {
                            NotificationUtil.sendSlackMessage("505: error during /verification/verify", "CF3U0LPDF");
                            res.status(202).json({
                                statusCode: 505,
                                timestamp: Date.now(),
                                message: "generating an auth token failed",
                                data: error
                            });
                        });
                    });
                }
            }
            else {
                res.status(202).json({
                    statusCode: 403,
                    timestamp: Date.now(),
                    message: "code is incorrect",
                    data: {
                        code: givenCode,
                        errorCode: "wrong_code",
                        username: username
                    }
                });
            }
        }
    });
});

module.exports = router;
