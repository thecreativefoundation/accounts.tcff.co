require('dotenv').config();

const accountSid = process.env.TWILIO_ACCOUNT_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;
const apiKey = process.env.MAILGUN_API_KEY;
const mailgun = require("mailgun-js")({apiKey: apiKey, domain: "mailgun.tcff.co"});
const tcffPhoneNumber = "+12246018233";
const twilioClient = require('twilio')(accountSid, authToken);

const VerificationCode = require("../models/verificationCode.model");

const NotificationUtil = require("./notification.util");

module.exports = {
    createCode: (username) => {
        NotificationUtil.sendSlackMessage("6: creating code for username: "+username, "CF3U0LPDF");
        let newCode = "";
        for (let i = 0; i < 6; i++) {
            const newNum = Math.floor(Math.random() * (9-0) + 1);
            newCode += newNum.toString();
        }
        return new Promise((resolve, error) => {
            VerificationCode({username: username, code: newCode}).save((err, codeData) => {
                if (err) {
                    console.log(err);
                    error(err);
                }
                else {
                    resolve({
                        username: codeData.username,
                        code: codeData.code
                    });
                }
            });
        });
    },
    sendCode: (method, code, recipient, username, body) => {
        NotificationUtil.sendSlackMessage("7: sending a code to "+username, "CF3U0LPDF");
        if (method === "email") {
            return new Promise((resolve, error) => {
                mailgun.messages().send({
                    to: "Jojo the Unicorn <jojo@tcff.co>",
                    from: recipient+", jojo@tcff.co",
                    subject: "TCF Accounts Verification Code",
                    text: body.replace("CODE", code).replace("USERNAME", username)
                }, (err, body) => {
                    if (err) {
                        error({
                            passed: 0,
                            data: err
                        });
                    }
                    else {
                        resolve({
                            passed: 1,
                            data: body
                        })
                    }
                });
            });
        }
        else if (method === "phone") {
            return new Promise((resolve, error) => {
                twilioClient.messages.create({
                    body: body.replace("CODE", code).replace("USERNAME", username),
                    to: recipient,
                    from: tcffPhoneNumber
                }).then((message) => {
                    if (message.sid.error_code === null) {
                        error({
                            passed: 0,
                            data: {
                                errorCode: message.sid.error_code,
                                errorMessage: message.sid.error_message
                            }
                        });
                    }
                    else {
                        resolve({
                            passed: 1,
                            data: message
                        });
                    }
                });
            });
        }
    }
};