require('dotenv').config();

const PORT = process.env.PORT || 80;
const express = require("express");
const Raven = require('raven');
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const MONGO_URL = process.env.MONGO_URL;

const AccountRoute = require("./routes/account.route");
const VerificationRoute = require("./routes/verification.route");
const AuthRoute = require("./routes/auth.route");
const HelpRoute = require("./routes/help.route");

Raven.config('https://34374039f74a49e7ba4168c5c57ab557@sentry.io/1265543').install();

const server = express();

mongoose.connect(MONGO_URL, { useNewUrlParser: true, autoIndex: false }).then(() => {
    console.log('mongodb connected');
}).catch((error) => {
    console.log(error);
});

mongoose.Promise = global.Promise;

server.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

server.use(bodyParser.json());
server.use(Raven.requestHandler());
server.use(Raven.errorHandler());

server.get("/", (req, res) => res.send("TCF ACCOUNTS API IS LIVE AND WELL"));

server.use("/account", AccountRoute);
server.use("/verification", VerificationRoute);
server.use("/auth", AuthRoute);
server.use("/help", HelpRoute);

server.listen(PORT, () => console.log("Listening on port " + PORT + "..."));
