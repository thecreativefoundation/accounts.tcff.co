module.exports = {
    buildSigninEmail: (ipAddress, username, fname) => {
        const template = `
        <div style="text-align: center">
            <img style="width: 50%" src="http://www.tcff.co/wp-content/uploads/2019/01/tcf-text-logo-cropped.png" />
            <h2>Hey {{FIRST_NAME}}, someone has signed in to your account</h2>
            <p>If it was you, then you have nothing to worry about. If not, there may be some suspicious activity on your account. Here are some things you can:</p>
            <ul>
                <li><a href="https://accounts.tcff.co/#/help/password">Reset your password</a></li>
                <li><a href="https://support.tcff.co/accounts/suspicious-activiy">Contact emergency support</a></li>
            </ul>
            <h3>Here's some more information:</h3>
            <ul>
                <li>Your Username: {{USERNAME}}</li>
                <li>IP Address: {{IP_ADDRESS}}</li>
                <li>Date: {{DATE}}</li>
                <li>Time: {{TIME}}</li>
            </ul>
            <p>Your friend,\nJojo The Unicorn</p>
        </div>`;

        const dateObj = new Date(Date.now());
        const month = dateObj.getMonth()+1;
        const day = dateObj.getDate();
        const year = dateObj.getFullYear();
        const hour = dateObj.getHours();
        const minute = dateObj.getMinutes();
        const date = month.toString()+"/"+day.toString()+"/"+year.toString();
        const time = hour.toString()+":"+minute.toString();

        return template
            .replace("{{IP_ADDRESS}}", ipAddress)
            .replace("{{TIME}}", time)
            .replace("{{DATE}}", date)
            .replace("{{USERNAME}}", username)
            .replace("{{FIRST_NAME}}", fname);
    },
    buildPasswordResetEmail: (ipAddress, username, fname) => {
        const template = `
        <div style="text-align: center">
            <img style="width: 50%" src="http://www.tcff.co/wp-content/uploads/2019/01/tcf-text-logo-cropped.png" />
            <h2>Hey {{FIRST_NAME}}, your TCF Account password has been changed</h2>
            <p>If it was you, then you have nothing to worry about. If not, there may be some suspicious activity on your account. Here are some things you can:</p>
            <ul>
                <li><a href="https://accounts.tcff.co/#/help/password">Reset your password</a></li>
                <li><a href="https://support.tcff.co/accounts/suspicious-activiy">Contact emergency support</a></li>
            </ul>
            <h3>Here's some more information:</h3>
            <ul>
                <li>Your Username: {{USERNAME}}</li>
                <li>IP Address: {{IP_ADDRESS}}</li>
                <li>Date: {{DATE}}</li>
                <li>Time: {{TIME}}</li>
            </ul>
            <p>Your friend,\nJojo The Unicorn</p>
        </div>`;

        const dateObj = new Date(Date.now());
        const month = dateObj.getMonth()+1;
        const day = dateObj.getDate();
        const year = dateObj.getFullYear();
        const hour = dateObj.getHours();
        const minute = dateObj.getMinutes();
        const date = month.toString()+"/"+day.toString()+"/"+year.toString();
        const time = hour.toString()+":"+minute.toString();

        return template
            .replace("{{IP_ADDRESS}}", ipAddress)
            .replace("{{TIME}}", time)
            .replace("{{DATE}}", date)
            .replace("{{USERNAME}}", username)
            .replace("{{FIRST_NAME}}", fname);
    },
    buildWelcomeEmail: (fname, username) => {
        const template = `
        <div style="text-align: center">
            <img style="width: 50%" src="http://www.tcff.co/wp-content/uploads/2019/01/tcf-text-logo-cropped.png" />
            <h2>Welcome to your TCF Account!</h2>
            <p>You're a full fledged member and hold the power to sign into any Creative service!</p>
            <p>Your friend,\nJojo The Unicorn</p>
 \n       </div>`;

        return template
            .replace("{{USERNAME}}", username)
            .replace("{{FIRST_NAME}}", fname);
    }
}
;