const router = require('express').Router();
const bcrypt = require("bcryptjs");

const AccountModel = require("../models/account.model");
const VerificationCode = require("../models/verificationCode.model");

const VerificationCodeUtil = require('../utils/verificationCode.util');
const TokenUtil = require("../utils/token.util");
const NotificationUtil = require("../utils/notification.util");
const UserActivityUtil = require("../utils/userActivity.util");
const EmailBuilderUtil = require("../utils/emailBuilder.util");

const callingCodes = require("../data/callingCodes.data");

router.post("/signup", (req, res) => {
    NotificationUtil.sendSlackMessage("10: a user is signing up", "CF3U0LPDF");
    const date = new Date(Date.now());
    bcrypt.genSalt(12, (err, salt) => {
        bcrypt.hash(req.body.password, salt, (err, hashedPassword) => {
            AccountModel({
                username: req.body.username.toLowerCase(),
                saltedPassword: hashedPassword,
                email: req.body.email,
                phone: "+"+callingCodes[req.body.ip.countryCode]+req.body.phoneNumber,
                meta: {
                    dateJoined: {
                        day: date.getDay(),
                        month: date.getMonth()+1,
                        year: date.getFullYear()
                    },
                    firstName: req.body.firstName,
                    lastName: req.body.lastName,
                    birthday: {
                        day: 0,
                        month: 0,
                        year: 0
                    }
                },
                profilePhotoUrl: "",
                verificationType: "phone",
                isVerified: false
            }).save((err, user) => {
                if (err) {
                    NotificationUtil.sendSlackMessage("505: error during /auth/signup", "CF3U0LPDF");
                    res.status(202).json({
                        statusCode: 505,
                        timestamp: Date.now(),
                        message: "sign up for username: "+req.body.username+" failed",
                        data: err
                    });
                }
                else {
                    VerificationCodeUtil.createCode(user.username).then((codeData) => {
                        console.log(codeData);
                        VerificationCodeUtil.sendCode("phone", codeData.code, user.phone, codeData.username, "Hey USERNAME,\n\nYour verification code is CODE to register your TCF Account\n\n- Jojo the Unicorn").then((response) => {
                            NotificationUtil.sendSlackMessage("10.5: a user signed up with username:"+req.body.username, "CF3U0LPDF");
                            UserActivityUtil.logActivity("signup", req.body.ip, req.body.username);
                            NotificationUtil.sendEmail(
                                EmailBuilderUtil.buildWelcomeEmail(
                                    req.body.firstName,
                                    req.body.username
                                ),
                                req.body.email,
                                "Welcome to your TCF Account"
                            );
                            res.status(202).json({
                                statusCode: 202,
                                timestamp: Date.now(),
                                message: "sign up passed, code is sent",
                                data: {
                                    username: codeData.username,
                                    contactMethod: "phone",
                                    response: response
                                }
                            })
                        }).catch((anotherErr) => {
                            NotificationUtil.sendSlackMessage("505: error during /auth/signup", "CF3U0LPDF");
                            res.status(202).json({
                                statusCode: 505,
                                timestamp: Date.now(),
                                message: "sending verification code failed",
                                data: anotherErr
                            });
                        });
                    }).catch((error) => {
                        NotificationUtil.sendSlackMessage("505: error during /auth/signup", "CF3U0LPDF");
                        res.status(202).json({
                            statusCode: 505,
                            timestamp: Date.now(),
                            message: "creating verification code failed",
                            data: error
                        });
                    });
                }
            });
        });
    });

});

router.post("/signin", (req, res) => {
    NotificationUtil.sendSlackMessage("11: a user is signing in with username:"+req.body.username, "CF3U0LPDF");
    AccountModel.findOne({'username': req.body.username}).exec((err, account) => {
        bcrypt.compare(req.body.password, account.saltedPassword).then((response) => {
            if (response === true) {
                UserActivityUtil.logActivity("signin", req.body.ip, req.body.username);
                NotificationUtil.sendEmail(
                    EmailBuilderUtil.buildSigninEmail(
                        req.body.ip,
                        req.body.username,
                        account.meta.firstName
                    ),
                    account.email,
                    "Someone has signed into your TCF Account"
                );
                VerificationCodeUtil.createCode(account.username).then((codeData) => {
                    VerificationCodeUtil.sendCode("phone", codeData.code, account.phone, account.username, "Hey USERNAME,\n\nYour verification code is CODE to sign in to your TCF Account\n\n- Jojo the Unicorn").then((response) => {
                        res.status(202).json({
                            statusCode: 202,
                            timestamp: Date.now(),
                            message: "sign in passed and code has been sent to the members phone",
                            data: {
                                username: account.username,
                                contactMethod: "phone",
                            }
                        });
                    }).catch((anotherError) => {
                        NotificationUtil.sendSlackMessage("505: error during /auth/signin", "CF3U0LPDF");
                        res.status(202).json({
                            statusCode: 505,
                            timestamp: Date.now(),
                            message: "sending verification code failed",
                            data: {
                                errorCode: "system_failure",
                                error: anotherError
                            }
                        });
                    });
                }).catch((error) => {
                    NotificationUtil.sendSlackMessage("505: error during /auth/signin", "CF3U0LPDF");
                    res.status(202).json({
                        statusCode: 505,
                        timestamp: Date.now(),
                        message: "creating verification code failed",
                        data: {
                            errorCode: "system_failure",
                            error: error
                        }
                    });
                });
            }
            else {
                res.status(202).json({
                    statusCode: 403,
                    timestamp: Date.now(),
                    message: "password is incorrect",
                    data: {
                        errorCode: "incorrect_credentials",
                    }
                });
            }
        });
    });
});

router.post("/reset_pass", (req, res) => {
    const givenCode = req.body.code;
    const username = req.body.username;
    const newPassword = req.body.newPassword;

    NotificationUtil.sendSlackMessage("12: reset password is being completed for username:"+username, "CF3U0LPDF");
    UserActivityUtil.logActivity("passwordResetCompletion", req.body.ip, username);

    VerificationCode.findOne({'username': username}).exec((error, data) => {
        if (error) {
            NotificationUtil.sendSlackMessage("505: error during /auth/reset_pass", "CF3U0LPDF");
            res.status(202).json({
                statusCode: 505,
                timestamp: Date.now(),
                message: "operation failed",
                data: error
            });
        }
        else {
            if (data.code === givenCode) {
                AccountModel.findOne({'username': username}).exec((error, account) => {
                    if (error) {
                        NotificationUtil.sendSlackMessage("505: error during /auth/reset_pass", "CF3U0LPDF");
                        res.status(202).json({
                            statusCode: 505,
                            timestamp: Date.now(),
                            message: "finding account information based on username failed",
                            data: error
                        });
                    }
                    else {
                        bcrypt.genSalt(12, (err, salt) => {
                            bcrypt.hash(newPassword, salt, (err, hashedPassword) => {
                                AccountModel.findByIdAndUpdate(account.id, {'saltedPassword': hashedPassword}).exec((err, newAccount) => {
                                    if (err) {
                                        NotificationUtil.sendSlackMessage("505: error during /auth/reset_pass", "CF3U0LPDF");
                                        res.status(202).json({
                                            statusCode: 505,
                                            timestamp: Date.now(),
                                            message: "updating new password failed",
                                            data: err
                                        });
                                    }
                                    else {
                                        VerificationCode.findByIdAndRemove(data.id).exec((error, info) => {
                                            let err = false;
                                            if (error) {
                                                err = error;
                                            }
                                            NotificationUtil.sendEmail(
                                                EmailBuilderUtil.buildPasswordResetEmail(
                                                    req.body.ip.query,
                                                    req.body.ip.location,
                                                    req.body.username,
                                                    account.meta.firstName
                                                ),
                                                account.email,
                                                "Someone has reset your TCF Account password"
                                            );
                                            res.status(202).json({
                                                statusCode: 202,
                                                timestamp: Date.now(),
                                                message: "password has been reset",
                                                data: {
                                                    username: username,
                                                    id: newAccount.id,
                                                    errorDuringRemoval: err,
                                                    info: info
                                                }
                                            });
                                        });
                                    }
                                });
                            });
                        });
                    }
                });
            }
            else {
                res.status(202).json({
                    statusCode: 302,
                    timestamp: Date.now(),
                    message: "code is incorrect",
                    data: {
                        code: givenCode,
                        username: username
                    }
                });
            }
        }
    });
});

router.post("/verify", (req, res) => {
    NotificationUtil.sendSlackMessage("13: application is verifing a users auth token username:"+req.body.username, "CF3U0LPDF");
    TokenUtil.decodeSecureJWT(req.body.token).then((data) => {
        if (data.username === req.body.username && data.iss === "The Creative Foundation") {
            res.status(202).json({
                statusCode: 202,
                timestamp: Date.now(),
                message: "user is who they say they are",
                data: data
            });
        }
        else {
            res.status(202).json({
                statusCode: 403,
                timestamp: Date.now(),
                message: "user is not who they say they are",
                data: {
                    errorCode: "unauthenticated"
                }
            });
        }
    }).catch((error) => {
        NotificationUtil.sendSlackMessage("505: error during /auth/verify", "CF3U0LPDF");
        res.status(202).json({
            statusCode: 505,
            timestamp: Date.now(),
            message: "unknown error",
            data: {
                error: error,
                errorCode: "system_failure"
            }
        });
    });
});

module.exports = router;